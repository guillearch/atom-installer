# AtoM GNU/Linux Installer

This script automates the installation of the **open source archival description software AtoM**, along with all the required third party components. At the end you should get AtoM running in a LEMP (GNU/Linux, Nginx, MySQL and PHP) environment. Some manual tasks will be needed during the process.

The lowest supported version is AtoM 2.4.

The code is licensed under the [GNU General Public License v3.0](https://github.com/guillearch/atom-installer/blob/master/LICENSE). Feel free to adapt the script for your needs!

## Requirements

These are the GNU/Linux distributions currently supported for each AtoM version:

- **2.8**: Ubuntu 20.04.
- **2.7**: Ubuntu 20.04.
- **2.6**: Ubuntu 18.04, Debian 10, Debian 11.
- **2.5**: Ubuntu 16.04/18.04, Debian 9.
- **2.4**: Ubuntu 16.04.

Note that you will have to run a web installer to complete the install.

Please make sure you have sudo privileges before continuing and you meet the minimum hardware requirements for the desired version, which are detailed in the [official documentation](https://www.accesstomemory.org/es/docs/).

## Downloading the installer

Clone the repository with the following command:

```
git clone https://gitlab.com/guillearch/atom-installer.git
```

## Running the script

Move to the script directory:

```
cd atom-installer/
```

Make the script executable:

```
sudo chmod +x atom-installer.sh
```

Run the script:

```
sudo ./atom-installer.sh
```

By default, the script installs the latest stable version. You can specify a version using the `-v` argument, followed by the desired version number:

```
sudo ./atom-installer.sh -v 2-4
```

Follow the instructions printed in the terminal.

During the installation, you will be asked which MySQL Server version do you wish to receive. **Make sure to select `mysql-8.0` for AtoM 2.6+**. Also, you will be prompted to set a password for the default administrator user (root) and to select the default authentication plugin, **which has to be set to “Use Legacy Authentication Method”**.

## Setting up AtoM

If you're installing AtoM 2.7+, the script will run a simple command line interface task that configures AtoM according to your environment, adds the necessary tables and initial data to the recently created database and creates the Elasticsearch index.

To configure the database, enter the following information:

- Database host: `localhost`
- Database port: `3306`
- Database name: `atom`
- Database user: `atom`
- Database password: `<your-password>`

To configure the search engine, enter the following information:

- Search host: `localhost`
- Search port: `9200`
- Search index: `atom`

Finally, the installer will ask you to enter a title, description and base URL for your site and set up the administrator account.

If you need to restart the setting up task, simply enter:

```
cd /usr/share/nginx/atom
sudo php symfony tools:install
```

## Running the web installer

For versions lower than 2.7, the script will prompt you to run the web installer in order to complete the installation. To do so, open your browser and type "localhost" in the address bar, if you're installing AtoM in a local machine; or type the IP address in the address bar if you're doing a remote installation.

## Enabling and starting the atom-worker service

If you're installing AtoM 2.5+, after running the web installer execute the following commands in order to enable and start the atom-worker service:

```
cd ~/atom-installer
sudo chmod +x atom-worker.sh && sudo ./atom-worker.sh
```

Note that this script must be executed after running the installer, as the service requires connection to the atom database.

## Contributors

- [Guillermo Castellano](https://gitlab.com/guillearch).
- [Elizabeth Oliva](https://gitlab.com/ElyPandora).

If you want to contribute to this project, please check our [contribution guidelines](https://gitlab.com/guillearch/atom-installer/blob/master/contributing.md) before submitting a new Pull Request.

## Additional information

This installer is based on the [AtoM documentation](https://www.accesstomemory.org/es/docs/) provided by Artefactual Systems.

Keep calm and use ISAD (G)!
