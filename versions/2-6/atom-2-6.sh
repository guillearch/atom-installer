#!/bin/bash
# AtoM 2.6 Installer

. /etc/os-release
version="$NAME $VERSION_ID"
case $version in
	"Ubuntu 18.04")
		chmod +x ./versions/2-6/atom-2-6-ubuntu-18-04.sh
		sudo sh versions/2-6/atom-2-6-ubuntu-18-04.sh;;
	"Debian GNU/Linux 10")
		chmod +x ./versions/2-6/atom-2-6-debian-10.sh
		sudo sh versions/2-6/atom-2-6-debian-10.sh;;
	"Debian GNU/Linux 11")
		chmod +x ./versions/2-6/atom-2-6-debian-11.sh
		sudo sh versions/2-6/atom-2-6-debian-11.sh;;	
	*)
		printf "\033[1;31mError: $version is not currently supported for AtoM 2.6.\033[0m\n"
		exit
esac
