#!/bin/bash
# AtoM 2.5 Installer

. /etc/os-release
version="$NAME $VERSION_ID"
case $version in
	"Ubuntu 18.04")
		chmod +x ./versions/2-5/atom-2-5-ubuntu-18-04.sh
		sudo sh versions/2-5/atom-2-5-ubuntu-18-04.sh;;
	"Ubuntu 16.04")
		chmod +x ./versions/2-5/atom-2-5-ubuntu-16-04.sh
		sudo sh versions/2-5/atom-2-5-ubuntu-16-04.sh;;
	"Debian GNU/Linux 9")
		chmod +x ./versions/2-5/atom-2-5-debian-9.sh
		sudo sh versions/2-5/atom-2-5-debian-9.sh;;
	*)
		printf "\033[1;31mError: $version is not currently supported for AtoM 2.5.\033[0m\n"
		exit
esac
