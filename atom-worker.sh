#!/bin/bash

# This script enables and starts the Gearman Job Worker for AtoM 2.7.
# It must be executed after running the web installer, as the worker requires connection to the atom database.

. /etc/os-release
version="$NAME $VERSION_ID"
case $version in
	"Ubuntu 20.04")
		php="php7.4";;
	"Ubuntu 18.04")
		php="php7.2";;
	"Ubuntu 16.04")
		php="php7.0";;
	"Debian GNU/Linux 9")
		php="php7.2";;
	"Debian GNU/Linux 10")
		php="php7.2";;
	"Debian GNU/Linux 11")
		php="php7.2";;
	*)
		printf "\033[1;31mError: $version is not currently supported for AtoM 2.8.\033[0m\n"
		exit
esac

# Create the service

touch /usr/lib/systemd/system/atom-worker.service
echo "[Unit]
Description=AtoM worker
After=network.target
# High interval and low restart limit to increase the possibility
# of hitting the rate limits in long running recurrent jobs.
StartLimitIntervalSec=24h
StartLimitBurst=3

[Install]
WantedBy=multi-user.target

[Service]
Type=simple
User=www-data
Group=www-data
WorkingDirectory=/usr/share/nginx/atom
ExecStart=/usr/bin/$php -d memory_limit=-1 -d error_reporting=\"E_ALL\" symfony jobs:worker
KillSignal=SIGTERM
Restart=on-failure
RestartSec=30" >> /usr/lib/systemd/system/atom-worker.service

# Reload systemd

systemctl daemon-reload

# Enables and starts service

systemctl enable atom-worker
systemctl start atom-worker
